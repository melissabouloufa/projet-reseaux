# Projet Réseaux

## Compilation
    make all 
    (compile serveur+client)


## Exécution 
    Serveur : ./serveur [PORT]
    Client  : ./client [ADRESSE] [PORT] [MODE]
              avec 
              -> mode = 1 si partie solo
              -> mode = 2 si partie équipe

## Touches de clavier (pendant une partie)
    flèches : déplacement du joueur
    *       : poser une bombe
    &       : annuler le dernier déplacement
    ctrl+c  : quitter la partie
    autres  : écrire un chat (envoyer avec entrée)